FROM debian:jessie

#installing php and nginx
RUN apt-get -y update && apt-get install -y \
	php5-cli\ 
	php5-common\ 
	php5-mysql\ 
	php5-gd\ 
	php5-fpm\ 
	php5-cgi\ 
	php5-fpm\ 
	php-pear\ 
	php5-mcrypt\ 
	php5-curl\ 
	php5-xdebug\
	nginx

#installing mysql server
RUN echo "mysql-server-5.6 mysql-server/root_password password password" | debconf-set-selections && \ 
	echo "mysql-server-5.6 mysql-server/root_password_again password password" | debconf-set-selections && \
	apt-get -y install mysql-server

#All parts are installed going to configurate image
ADD configs /usr/local/configs/
RUN chmod -Rf 777 /usr/local/configs &&\
	/usr/local/configs/configurate.sh

EXPOSE 80

CMD ["/bin/bash"]