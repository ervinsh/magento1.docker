#!/usr/bin/env bash
mysql -u root -ppassword -e "UPDATE magento.core_config_data SET value = 'http://$1/' WHERE core_config_data.path = 'web/unsecure/base_url'"
  mysql -u root -ppassword -e "UPDATE magento.core_config_data SET value = 'https://$1/' WHERE core_config_data.path = 'web/secure/base_url'"
