INSERT INTO admin_user
  (email, username, password, is_active)
VALUE
  ('maven@mavenecommerce.com','magento', (SELECT MD5('password')), 1)
ON DUPLICATE  KEY UPDATE
  username = 'magento', password =(SELECT MD5('password')),is_active=1;

# Creating default admin-user group if not existing
INSERT IGNORE INTO admin_role
    (parent_id,tree_level,sort_order,role_type,user_id,   role_name)
VALUES (
        0,        1,        1,          'G',      0,    'Administrators'
);

# Creating our account to access all things in admin panel
INSERT INTO admin_role
    (parent_id,tree_level,sort_order,role_type,user_id,role_name)
VALUES
(       1,        2,          0,        'U',
    (SELECT admin_user.user_id FROM admin_user WHERE username = 'magento'),
    'Super User'
);
