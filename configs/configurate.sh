#!/bin/bash
./local.sh

echo "---------Configuring nginx--------------"
cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=m1.dev" -keyout /etc/nginx/ssl/d.key -out /etc/nginx/ssl/m1.crt

cp /usr/local/configs/nginx/nginx.conf    /etc/nginx/nginx.conf
cp /usr/local/configs/nginx/magento.conf  /etc/nginx/sites-available/magento.conf

ln -s /etc/nginx/sites-available/magento.conf /etc/nginx/sites-enabled/magento.conf
rm -rf /etc/nginx/sites-enabled/default

sudo cp /vagrant/provision/php-fpm/www.conf /etc/php5/fpm/pool.d/www.conf
sudo cp /vagrant/provision/php-fpm/20-xdebug.ini /etc/php5/fpm/conf.d/20-xdebug.ini

